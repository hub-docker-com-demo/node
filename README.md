# [node](https://hub.docker.com/_/node/)

Node.js is a JavaScript-based platform for server-side and networking applications.

(Unofficial demo and howto)

## Contents
* Trivial "yarn" and "npm" usage
* Applies concepts form https://gitlab.com/hub-docker-com-demo/alpine
  * Using "node" user and "entrypoint" to execute the shell as non-root.
  * Allowing the non-root user to use sudo.
  * Installing (apk) packages before executing the (yaml) script.
  * Timing packages installation and yaml script execution time with "time".
  * See [.gitlab-ci.yml](httpshttps://gitlab.com/hub-docker-com-demo/node/blob/master/.gitlab-ci.yml) file.

## [yarn](https://yarnpkg.com/) official documentation
* [Usage](https://yarnpkg.com/en/docs/usage)
* [Documentation](https://yarnpkg.com/en/docs)
* [CLI](https://yarnpkg.com/en/docs/cli/)